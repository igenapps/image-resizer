local Library = require "CoronaLibrary"

-- Create library
local lib = Library:new{ name='plugin_IGenAppsImageUtil', publisherId='com.IGenApps' }

-------------------------------------------------------------------------------
-- BEGIN (Insert your implementation starting here)
-------------------------------------------------------------------------------

-- This sample implements the following Lua:
-- 
--    local IGenAppsImageUtil = require "plugin_IGenAppsImageUtil"
--    local function listener( event )
--		native.showAlert( event.name, event.message, { "OK" } )
--	  end
--    IGenAppsImageUtil.init(listener)
--    
lib.init = function(listener)
	native.showAlert( 'ERROR', 'plugin_IGenAppsImageUtil.init() not implemented', { 'OK' } )
end
lib.displayImagePicker = function(listener)
	native.showAlert( 'ERROR', 'plugin_IGenAppsImageUtil.displayImagePicker() not implemented', { 'OK' } )
end
lib.createNewScalePngImageWithThumbnail = function(path, fileName, newImageSize, thumbnailWidth, thumbnailHeight, listener)
	native.showAlert( 'ERROR', 'plugin_IGenAppsImageUtil.createNewScalePngImageWithThumbnail() not implemented', { 'OK' } )
end

-------------------------------------------------------------------------------
-- END
-------------------------------------------------------------------------------

-- Return an instance
return lib
