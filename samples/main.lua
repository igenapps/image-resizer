local library = require "plugin.IGenAppsImageUtil"

-- This event is dispatched to the global Runtime object
-- by `didLoadMain:` in MyCoronaDelegate.mm

local imagePath = nil

local function delegateListener( event )
	native.showAlert(
		"Event dispatched from `didLoadMain:`",
		"of type: " .. tostring( event.name ),
		{ "OK" } )
end
Runtime:addEventListener( "delegate", delegateListener )

-- This event is dispatched to the following Lua function
-- by PluginLibrary::show() in PluginLibrary.mm
local function listener( event )
    imagePath = event.imageName;
end

library.init( listener )



local button = display.newRect(120,240,80,70)
button:setFillColor(255, 0, 0)
local button2 = display.newRect(120,100,80,70)
button2:setFillColor(0, 255, 0)

local displayImagePickerlistener = function ( event )
    library.createNewScalePngImageWithThumbnail(event.path,"myFile", 1920, 300 , 300)
end

-- Button tap listener
local function pickPhoto( event )
local path = system.pathForFile(  "data.txt", system.DocumentsDirectory )
	--native.showAlert( event.name, path, { "OK" } )
    library.displayImagePicker(displayImagePickerlistener)
end

local function displayImage(event)
    if imagePath then
        local myImage = display.newImage( imagePath , system.DocumentsDirectory , true)
        myImage:toBack()
        button:removeSelf()
        button2:removeSelf()
        if myImage then
            native.showAlert("INFO",  "myImage W,H " .. myImage.width .. "," .. myImage.height, { "OK" } )
        end
    end
end

button:addEventListener("tap", pickPhoto )
button2:addEventListener("tap", displayImage )