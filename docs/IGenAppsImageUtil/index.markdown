# IGemAppImageUtil.*

> --------------------- ------------------------------------------------------------------------------------------
> __Type__              [library][api.type.library]
> __Revision__          [2013.1202](http://developer.anscamobile.com/release/2013/1202)
> __Keywords__          IGemAppImageUtil
> __Sample code__       
> __See also__          
> __Availability__      Pro, Enterprise
> --------------------- ------------------------------------------------------------------------------------------

## Overview

This library provides method to load and scale images from user photo album bigger than the device full screen size.

## Platforms

* Android: Yes
* iOS: Yes
* Mac: No
* Win: No
* Kindle: No
* NOOK: No

## Syntax

	local IGemAppImageUtil = require "plugin.IGenAppsImageUtil"

## Functions

#### [IGenAppsImageUtil.init()][plugin.IGenAppsImageUtil.init]
	Init library callback listener for createNewScalePngImageWithThumbnail.

#### [IGenAppsImageUtil.displayImagePicker()][plugin.IGenAppsImageUtil.displayImagePicker]
	Display native photo picker on Android or IOS. Selected image path will be return in listener function event.path.
	 
#### [IGenAppsImageUtil.createNewScalePngImageWithThumbnail()][plugin.IGenAppsImageUtil.createNewScalePngImageWithThumbnail]
	Create new scale png image and thumbnail of asset located at path. The new png image will preserve aspect ratio with one dimension set to newImageSize. 
	Listener function event map will contain the following keys {imagePath, thumbnailImagePath, imageName, thumbnailImageName, baseDirectory} 

### SDK

When you build using the Corona Simulator, the server automatically takes care of integrating the plugin into your project. 

All you need to do is add an entry into a `plugins` table of your `build.settings`. The following is an example of a minimal `build.settings` file:

``````
settings =
{
	plugins =
	{
		-- key is the name passed to Lua's 'require()'
		["plugin.IGenAppsImageUtil"] =
		{
			-- required
			publisherId = "com.IGenApps",
		},
	},		
}
``````

### Enterprise

TBD

## Sample Code

``````
local library = require "plugin.IGenAppsImageUtil"

local function listener( event )
    local imagePath = event.imageName;
    if imagePath then
        local myImage = display.newImage( imagePath , system.DocumentsDirectory , true)
        myImage:toBack()
        if myImage then
            native.showAlert("INFO",  "myImage W,H " .. myImage.width .. "," .. myImage.height, { "OK" } )
        end
    end
end

library.init( listener )


local button = display.newRect(120,240,80,70)
button:setFillColor(255, 0, 0)


local displayImagePickerlistener = function ( event )
    library.createNewScalePngImageWithThumbnail(event.path,"myFile", 1920, 300 , 300)
end

local function pickPhoto( event )
    library.displayImagePicker(displayImagePickerlistener)
end

button:addEventListener("tap", pickPhoto )

``````
## Support

More support is available from the IGenAppsImageUtil team:

* [E-mail](mailto://info@igenapps.com)
* [Forum](http://forum.coronalabs.com/plugin/IGenAppsImageUtil)
* [Plugin Publisher](http://igenapps.com)


