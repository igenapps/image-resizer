# IGenAppsImageUtil.displayImagePicker()

> --------------------- ------------------------------------------------------------------------------------------
> __Type__              [function][api.type.function]
> __Library__           [IGenAppsImageUtil.*][plugin.IGenAppsImageUtil]
> __Revision__          [2013.1202](http://developer.anscamobile.com/release/2013/1202)
> __Keywords__          
> __Sample code__       
> __See also__          
> --------------------- ------------------------------------------------------------------------------------------


## Overview
	
	Display native photo picker on Android or IOS. Selected image path will be return in listener function event.path.

## Syntax

	IGenAppsImageUtil.displayImagePicker( listener )
	
##### listener ~^(required)^~
_[Listener][api.type.TYPE]._ Listener function with selected image path in event.path.

## Examples

``````lua
local library = require "plugin.IGenAppsImageUtil"
local displayImagePickerlistener = function ( event )
    print("Selected image path" .. event.path)
end
library.displayImagePicker(displayImagePickerlistener)
``````
