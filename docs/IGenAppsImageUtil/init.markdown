# IGenAppsImageUtil.init()

> --------------------- ------------------------------------------------------------------------------------------
> __Type__              [function][api.type.function]
> __Library__           [IGenAppsImageUtil.*][plugin.IGenAppsImageUtil]
> __Revision__          [2013.1202](http://developer.anscamobile.com/release/2013/1202)
> __Keywords__          
> __Sample code__       
> __See also__          
> --------------------- ------------------------------------------------------------------------------------------


## Overview
	
	Setup callback listener for IGenAppsImageUtil.createNewScalePngImageWithThumbnail() function.

## Syntax

	IGenAppsImageUtil.init( listener )

##### listener ~^(required)^~
_[Listener][api.type.TYPE]._ Listener for IGenAppsImageUtil.createNewScalePngImageWithThumbnail() function.


## Examples

``````lua
local library = require "plugin.IGenAppsImageUtil"
local function listener( event )
    local imagePath = event.imageName;
end

library.init( listener )
``````
