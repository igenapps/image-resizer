# IGenAppsImageUtil.createNewScalePngImageWithThumbnail()

> --------------------- ------------------------------------------------------------------------------------------
> __Type__              [function][api.type.function]
> __Library__           [IGenAppsImageUtil.*][plugin.IGenAppsImageUtil]
> __Revision__          [2013.1202](http://developer.anscamobile.com/release/2013/1202)
> __Keywords__          
> __Sample code__       
> __See also__          
> --------------------- ------------------------------------------------------------------------------------------


## Overview
	
	Create new scale png image and thumbnail of asset located at path. The new png image will preserve aspect ratio with one dimension set to newImageSize. 
	Listener function event map will contain the following keys {imagePath, thumbnailImagePath, imageName, thumbnailImageName, baseDirectory} 

## Syntax

	IGenAppsImageUtil.createNewScalePngImageWithThumbnail( path, fileName, newImageSize, thumbnailWidth, thumbnailHeight)
	

##### path ~^(required)^~
_[String][api.type.TYPE]._ Image Path.

##### fileName ~^(required)^~
_[String][api.type.TYPE]._ New png image name.

##### newImageSize ~^(required)^~
_[Number][api.type.TYPE]._ Size of width or height of new image.

##### thumbnailWidth ~^(required)^~
_[Number][api.type.TYPE]._ Width of new image thumbnail.

##### thumbnailHeight ~^(required)^~
_[Number][api.type.TYPE]._ Height of new image thumbnail.

## Examples

``````lua
local library = require "plugin.IGenAppsImageUtil"
local library = require "plugin.IGenAppsImageUtil"
local function listener( event )
	local imagePath = event.imagePath;
    local thumbnailImagePath = event.thumbnailImagePath;
    local imageName = event.imageName;
    local imageName = event.thumbnailImageName;
    local baseDirectory = event.baseDirectory;
end
local displayImagePickerlistener = function ( event )
    library.createNewScalePngImageWithThumbnail(event.path,"myFile", 1920, 300 , 300)
end
library.displayImagePicker(displayImagePickerlistener)

``````
